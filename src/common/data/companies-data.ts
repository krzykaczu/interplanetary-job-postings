import { Company } from "../interfaces";

export const companiesData: Company[] = [
  {
    key: "1",
    companyName: "Taikai",
    logotype: "assets/logotypes/taikai.png",
    about:
      "TAIKAI is an open innovation platform that works with organizations, universities and event organizers from around the world helping them to organize innovation competitions and hackathons.",
  },
  {
    key: "2",
    companyName: "Spacex",
    logotype: "assets/logotypes/spacex.png",
    about:
      "Space Exploration Technologies Corp. (SpaceX) is an American aerospace manufacturer and space transportation services company headquartered in Hawthorne, California. ",
  },
  {
    key: "3",
    companyName: "NASA",
    logotype: "assets/logotypes/nasa.png",
    about:
      "The National Aeronautics and Space Administration is an independent agency of the U.S. federal government responsible for the civilian space program, as well as aeronautics and space research.",
  },
  {
    key: "4",
    companyName: "HAL Laboratories",
    logotype: "assets/logotypes/hal9000.jpg",
    about:
      "HAL Laboratories produces and sells computer hardware, middleware and software, and provides hosting and consulting services in areas ranging from mainframe computers to nanotechnology.",
  },
  {
    key: "5",
    companyName: "United Federation of Planets",
    logotype: "assets/logotypes/federation.png",
    about:
      "Founded in 2161, the United Federation of Planets is an interstellar alliance of more than 150 planetary governments, spread out over 8,000 light-years. Members of the Federation are united in various endeavors involving trade, exploration, science and defense. The Federation is overseen by the Federation Council, which is comprised of representatives from member planets.",
  },
  {
    key: "6",
    companyName: "Guardians of the Galaxy",
    logotype: "assets/logotypes/guardians.png",
    about:
      "Group of independent bounty hunting contractors, typically taking assignments from bail bond agencies",
  },
];
